<?php

namespace Infotechnohelp\TextUnit;

/**
 * Class CodeTextUnit
 * @package Infotechnohelp\TextUnit
 */
class CodeTextUnit extends TextUnit
{
    private $dependencies;

    public function __construct(callable $template, $scope = null, callable $dependencies = null)
    {
        parent::__construct($template, $scope);

        $this->dependencies = $dependencies;
    }

    public function getDependencies()
    {
        $callable = $this->dependencies;

        return $callable($this->getScope());
    }
}
