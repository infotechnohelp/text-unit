<?php

namespace Infotechnohelp\TextUnit;

use Infotechnohelp\Scope\Scope;

/**
 * Class TextUnit
 * @package Infotechnohelp\TextUnit
 */
class TextUnit
{
    /**
     * @var \Infotechnohelp\Scope\Scope
     */
    private $scope;

    /**
     * @var callable
     */
    private $template;

    /**
     * TextUnit constructor.
     *
     * @param callable                         $template
     * @param \Infotechnohelp\Scope\Scope|null $scope
     */
    public function __construct(callable $template, Scope $scope = null)
    {
        $this->template = $template;
        $this->scope    = $scope;
    }

    /**
     * @return \Infotechnohelp\Scope\Scope|null
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @return string
     */
    public function init(): string
    {
        $callable = $this->template;

        return $callable($this->scope);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->init();
    }
}
