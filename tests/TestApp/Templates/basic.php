<?php

$template = function (\Infotechnohelp\Scope\AssociativeScope $scope) {
    $result = '';

    $result .= "use App\\Model\\Entity\\" . (new \Infotechnohelp\Modified\Modified($scope->get('tableTitle')))->singular() . ";\n\n";

    $result .= "/**\n";
    /** @var \Infotechnohelp\Scope\AssociativeScope $field */
    foreach ($scope->get('fields') as $field) {
        $result .= " * @param ";
        if ($field->get('type') != null) {
            $type = $field->get('type');
            if ($type == 'integer') {
                $type = 'int';
            }

            $result .= $type;
            if ($field->get('default') == 'null') {
                $result .= "|null";
            }
            $result .= " ";
        }
        $title  = $field->get('title');
        $result .= "\$$title\n";
    }

    $result .= " *\n";

    $result .= " * @return \\App\Model\\Entity\\" . (new \Infotechnohelp\Modified\Modified($scope->get('tableTitle')))->singular() . ";\n";

    $result .= " */";

    return $result;
};