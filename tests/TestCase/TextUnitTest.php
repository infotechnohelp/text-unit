<?php

namespace Infotechnohelp\TextUnit\Test\TestCase;

use Infotechnohelp\Modified\Modified;
use Infotechnohelp\Scope\AssociativeScope;
use Infotechnohelp\Scope\IndexedScope;
use Infotechnohelp\Scope\Scope;
use Infotechnohelp\Scope\ScopeUnitWithIndex;
use Infotechnohelp\Scope\Interfaces\IndexedScopeItem;
use Infotechnohelp\TextUnit\TextUnit;
use PHPUnit\Framework\TestCase;

/**
 * Class TextUnitTest
 * @package Infotechnohelp\TextUnit\Test\TestCase
 */
class TextUnitTest extends TestCase
{
    public function testBasic()
    {
        $scope = new IndexedScope(['My', 'name', 'is', 'Philipp',]);

        $template = function (Scope $scope = null) {
            $result = '';
            /** @var IndexedScopeItem|ScopeUnitWithIndex $item */
            foreach ($scope as $item) {
                if ($item->isLast()) {
                    $result .= $item;
                    continue;
                }
                $result .= "$item\n";
            }

            return $result;
        };

        $this->assertEquals("My\nname\nis\nPhilipp", (new TextUnit($template, $scope))->init());
        $this->assertEquals("My\nname\nis\nPhilipp", new TextUnit($template, $scope));
    }


    public function testWorkflow()
    {
        $rootScope = new AssociativeScope([
            'tableTitle' => 'Users',
            'fields'     => [
                [
                    'title'   => 'hostname',
                    'type'    => 'string',
                    'default' => null,
                ],
                [
                    'title'   => 'username',
                    'type'    => 'string',
                    'default' => null,
                ],
                [
                    'title'   => 'password',
                    'type'    => 'string',
                    'default' => null,
                ],
                [
                    'title'   => 'port',
                    'type'    => 'integer',
                    'default' => 'null',
                ],
            ],
        ]);


        $template = function (AssociativeScope $scope) {
            $result = '';

            $result .= "use App\\Model\\Entity\\" . (new Modified($scope->get('tableTitle')))->singular() . ";\n\n";

            $result .= "/**\n";
            /** @var AssociativeScope $field */
            foreach ($scope->get('fields') as $field) {
                $result .= " * @param ";
                if ($field->get('type') != null) {
                    $type = $field->get('type');
                    if ($type == 'integer') {
                        $type = 'int';
                    }
                    $result .= $type;
                    if ($field->get('default') == 'null') {
                        $result .= "|null";
                    }
                    $result .= " ";
                }
                $title  = $field->get('title');
                $result .= "\$$title\n";
            }

            $result .= " *\n";

            $result .= " * @return \\App\Model\\Entity\\" . (new Modified($scope->get('tableTitle')))->singular() . ";\n";

            $result .= " */";

            return $result;
        };


        $expected = "use App\Model\Entity\User;

/**
 * @param string \$hostname
 * @param string \$username
 * @param string \$password
 * @param int|null \$port
 *
 * @return \App\Model\Entity\User;
 */";

        $this->assertEquals($expected, (new TextUnit($template, $rootScope))->init());
    }

    public function testWorkflowWithIncludedTemplate()
    {
        $rootScope = new AssociativeScope([
            'tableTitle' => 'Users',
            'fields'     => [
                [
                    'title'   => 'hostname',
                    'type'    => 'string',
                    'default' => null,
                ],
                [
                    'title'   => 'username',
                    'type'    => 'string',
                    'default' => null,
                ],
                [
                    'title'   => 'password',
                    'type'    => 'string',
                    'default' => null,
                ],
                [
                    'title'   => 'port',
                    'type'    => 'integer',
                    'default' => 'null',
                ],
            ],
        ]);

        $expected = "use App\Model\Entity\User;

/**
 * @param string \$hostname
 * @param string \$username
 * @param string \$password
 * @param int|null \$port
 *
 * @return \App\Model\Entity\User;
 */";

        include(getcwd() . '/tests/TestApp/Templates/basic.php');
        /** @var $template */
        $this->assertEquals($expected, (new TextUnit($template, $rootScope))->init());
    }
}
