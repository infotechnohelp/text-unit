<?php

namespace Infotechnohelp\TextUnit\Test\TestCase;

use Infotechnohelp\Modified\Modified;
use Infotechnohelp\Scope\IndexedScope;
use Infotechnohelp\Scope\Scope;
use Infotechnohelp\Scope\ScopeUnitWithIndex;
use Infotechnohelp\Scope\Interfaces\IndexedScopeItem;
use Infotechnohelp\TextUnit\CodeTextUnit;
use PHPUnit\Framework\TestCase;

/**
 * Class CodeTextUnitTest
 * @package Infotechnohelp\TextUnit\Test\TestCase
 */
class CodeTextUnitTest extends TestCase
{
    public function testBasic()
    {
        $scope = new IndexedScope(['My', 'name', 'is', 'Philipp',]);

        $template = function (Scope $scope = null) {
            $result = '';
            /** @var IndexedScopeItem|ScopeUnitWithIndex $item */
            foreach ($scope as $item) {
                if ($item->isLast()) {
                    $result .= $item;
                    continue;
                }
                $result .= "$item\n";
            }

            return $result;
        };

        $this->assertEquals("My\nname\nis\nPhilipp", (new CodeTextUnit($template, $scope))->init());
        $this->assertEquals("My\nname\nis\nPhilipp", new CodeTextUnit($template, $scope));
    }

    public function testDependencies()
    {
        $scope = new IndexedScope(['Users', 'UserMailboxes']);

        $dependencies = function (IndexedScope $scope) {
            $result = [];
            /** @var \Infotechnohelp\Scope\ScopeUnit $first */
            $first    = $scope->first();
            $result[] = "use App\\Model\\Table\\" . $first . ";";
            $result[] = "use App\\Model\\Entity\\" . (new Modified($first))->singular() . ";";

            /** @var \Infotechnohelp\Scope\ScopeUnit $last */
            $last     = $scope->last();
            $result[] = "use App\\Model\\Table\\" . $last . ";";
            $result[] = "use App\\Model\\Entity\\" . (new Modified($last))->singular() . ";";

            return $result;
        };

        $expected = [
            'use App\Model\Table\Users;',
            'use App\Model\Entity\User;',
            'use App\Model\Table\UserMailboxes;',
            'use App\Model\Entity\UserMailbox;',
        ];

        $CodeTextUnit = new CodeTextUnit(function () {
        }, $scope, $dependencies);

        $this->assertEquals($expected, $CodeTextUnit->getDependencies());
    }
}
